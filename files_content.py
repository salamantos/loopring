DEPLOY_2 = """var ConvertLib = artifacts.require("./%s");
var Subfile = artifacts.require("./%s");

module.exports = function(deployer) {
  deployer.deploy(ConvertLib);
  deployer.link(ConvertLib, Subfile);
  deployer.deploy(Subfile);
};
"""

TEST_1 = """pragma solidity ^0.4.2;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/%s";

contract Test%s {

    function testMainFunction() public {
        %s obj = %s(DeployedAddresses.%s());

        Assert.equal(obj.check(), true, "Something went wrong");
    }

}
"""

MAIN = """pragma solidity ^0.4.24;

import "./%s";
pragma experimental "v0.5.0";
pragma experimental "ABIEncoderV2";

contract %s {
    using %s for bytes;
    using %s for *;

    function check() public returns(bool theTruth){
        SimpleOrderLib.SimpleOrder memory obj1 = SimpleOrderLib.SimpleOrder(%s);
        bytes memory output = obj1.toBytes();
        SimpleOrderLib.SimpleOrder memory obj2 = SimpleOrderLib.toSimpleOrder(output);
        %s
    }
}
"""

LIBRARY = """pragma solidity ^0.4.24;
pragma experimental "v0.5.0";
pragma experimental "ABIEncoderV2";
//import "path/to/Prosol.sol"; // this has some basic methods.

/// Automatically generated from simple_order.prosol
/// Do not change manually.

library %s {
    %s

    struct %s {
        %s
    }
    
    function toBytes(%s obj) public pure returns (bytes output) {
        bytes memory _data = new bytes(0);

        %s
        return (_data);
    }
    function to%s(bytes input) public pure returns (%s obj) {
        uint total_len = 0;

        %s
    }
}
"""