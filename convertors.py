FUNCTIONS = """function concat(bytes memory a, bytes memory b) public pure returns (bytes memory c) {
        // Store the length of the first array
        uint alen = a.length;
        // Store the length of BOTH arrays
        uint totallen = alen + b.length;
        // Count the loops required for array a (sets of 32 bytes)
        uint loopsa = (a.length + 31) / 32;
        // Count the loops required for array b (sets of 32 bytes)
        uint loopsb = (b.length + 31) / 32;
        assembly {
            let m := mload(0x40)
            // Load the length of both arrays to the head of the new bytes array
            mstore(m, totallen)
            // Add the contents of a to the array
            for {  let i := 0 } lt(i, loopsa) { i := add(1, i) } { mstore(add(m, mul(32, add(1, i))), mload(add(a, mul(32, add(1, i))))) }
            // Add the contents of b to the array
            for {  let i := 0 } lt(i, loopsb) { i := add(1, i) } { mstore(add(m, add(mul(32, add(1, i)), alen)), mload(add(b, mul(32, add(1, i))))) }
            mstore(0x40, add(m, add(32, totallen)))
            c := m
        }
    }


    function addressToBytes(address a) public pure returns (bytes b){
        assembly {
            let m := mload(0x40)
            mstore(add(m, 20), xor(0x140000000000000000000000000000000000000000, a))
            mstore(0x40, add(m, 52))
            b := m
        }
    }

    function uint256ToBytes(uint256 x)  public pure returns (bytes b) {
        b = new bytes(32);
        assembly { mstore(add(b, 32), x) }
    }
    
    function bytesToUint(bytes bs, uint start) internal pure returns (uint)
    {
        require(bs.length >= start + 32, "slicing out of range");
        uint x;
        assembly {
            x := mload(add(bs, add(0x20, start)))
        }
        return x;
    }

    function bytesToAddress (bytes b, uint start) public pure returns (address) {
        uint result = 0;
        for (uint i = start; i < start + 20; i++) {
            uint c = uint(b[i]);
            result = result * 256 + c;
        }
        return address(result);
    }
    
    """

ADDRESS_TO_BYTES = """bytes memory _data%s = new bytes(20);
        _data%s = addressToBytes(obj.%s);
        _data = concat(_data, _data%s);
        """

UINT_TO_BYTES = """bytes memory _data%s = new bytes(32);
        _data%s = uint256ToBytes(obj.%s);
        _data = concat(_data, _data%s);
        """

BYTES_TO_UINT = """obj.%s = bytesToUint(input, total_len);
        total_len += 32;
        """

BYTES_TO_ADDRESS = """obj.%s = bytesToAddress(input, total_len);
        total_len += 20;
        """
