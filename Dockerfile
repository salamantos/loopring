FROM ubuntu

RUN apt-get update -y
RUN apt-get install -y git npm nodejs screen
RUN npm install -g truffle

ADD generator.py /
ADD files_content.py /
ADD convertors.py /
# ADD library/* /library/
ADD sourse.prosol /
ADD library/contracts/Migrations.sol /library/contracts/
ADD library/migrations/1_initial_migration.js /library/migrations/
ADD library/test/placeholder /library/test/
ADD library/truffle-config.js /library/
ADD library/truffle.js /library/

RUN python generator.py
RUN cd library
# RUN chmod -R 777 .

# ENTRYPOINT truffle test
ENTRYPOINT cd library && truffle test
