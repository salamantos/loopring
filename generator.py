import os
import re

import files_content, convertors

DIR_PREFIX = 'library/'

to_bytes_generators = ''
test_args = ''
def type_to_bytes(type, field_name):
    global to_bytes_generators
    global test_args
    if type == 'address':
        to_bytes_generators += convertors.ADDRESS_TO_BYTES % (field_name, field_name, field_name, field_name)
        test_args += '0xE0f5206BBD039e7b0592d8918820024e2a7437b9, '
    if type == 'uint':
        to_bytes_generators += convertors.UINT_TO_BYTES % (field_name, field_name, field_name, field_name)
        test_args += '146, '

from_bytes_generators = ''
def bytes_to_type(type, field_name):
    global from_bytes_generators
    if type == 'address':
        from_bytes_generators += convertors.BYTES_TO_ADDRESS % (field_name)
    if type == 'uint':
        from_bytes_generators += convertors.BYTES_TO_UINT % (field_name)

sourse_code = ''
with open('sourse.prosol') as sourse_file:
    for line in sourse_file:
        sourse_code += line

# parse .prosol file
try:
    struct_name = re.search(r'structure ([\w_-]+)', sourse_code).group(1)
except Exception as e:
    print('Not found structure name (%s)' % e)
    exit(1)

try:
    raw_args = re.search(r'\{([.\t\n\r\w\s\=\;]+)\}', sourse_code).group(1)
    args = re.sub(r'(\s)*\=(\s)*(\d)+\;', ';', raw_args).lstrip().rstrip()
except Exception as e:
    print('Not found fields (%s)' % e)
    exit(1)

print('Found stucture ' + struct_name)
print('With fields ' + args)

# # create main files
# dirs = ['contract/' + struct_name + '.sol',
#         'contract/' + struct_name + 'Lib' + '.sol',
#         'migrations/2_deploy_contract.js',
#         'test/check.sol'
#         ]
# for dir in dirs:
#     if not os.path.exists(DIR_PREFIX + dir):
#         os.makedirs(DIR_PREFIX + dir)

# create generators
fields_lines = re.findall(r'[\w\s]+;', args)
fields = []
for field in fields_lines:
    field = field.lstrip().rstrip()
    type, name = re.findall(r'\w+', field)
    fields.append({'type': type, 'name': name})

check_structs = 'return '
for field in fields:
    type_to_bytes(field['type'], field['name'])
    bytes_to_type(field['type'], field['name'])
    check_structs += 'obj1.' + field['name'] + ' == obj2.' + field['name'] + ' && '

test_args = test_args[:-2]
check_structs = check_structs[:-4] + ';'

# generate main files
with open(DIR_PREFIX + 'migrations/2_deploy_contract.js', 'w+') as f:
    f.write(files_content.DEPLOY_2 % (struct_name + 'Lib.sol', struct_name + '.sol'))
with open(DIR_PREFIX + 'test/Test' + struct_name + '.sol', 'w+') as f:
    f.write(files_content.TEST_1 % (struct_name + '.sol', struct_name, struct_name, struct_name, struct_name))
with open(DIR_PREFIX + 'contracts/' + struct_name + '.sol', 'w+') as f:
    f.write(files_content.MAIN % (struct_name + 'Lib.sol', struct_name, struct_name + 'Lib', struct_name + 'Lib', test_args, check_structs))
with open(DIR_PREFIX + 'contracts/' + struct_name + 'Lib.sol', 'w+') as f:
    f.write(files_content.LIBRARY % (struct_name + 'Lib', convertors.FUNCTIONS, struct_name, args, struct_name, to_bytes_generators, struct_name, struct_name,from_bytes_generators))

print('===== Compiled! =====')




